Name:		libseccomp
Version:	2.6.0
Release:        1
Summary:	Interface to the syscall filtering mechanism
License:	LGPL-2.1-only
URL:		https://github.com/seccomp/libseccomp
Source0:	https://github.com/seccomp/libseccomp/releases/download/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	gcc gperf

%description
The libseccomp library provides an easy to use, platform independent, interface to
the Linux Kernel's syscall filtering mechanism. The libseccomp API is designed to
abstract away the underlying BPF based syscall filter language and present a more
conventional function-call based filtering interface that should be familiar to,
and easily adopted by, application developers.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Provides:       %{name}-static = %{version}-%{release}
Obsoletes:      %{name}-static <= %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --enable-static
%make_build

%install
%make_install
%delete_la

%check
#Temporarily disable check on loongarch64
%ifnarch loongarch64
%make_build check
%endif

%files
%doc CREDITS
%license LICENSE
%{_libdir}/%{name}.so.*

%files devel
%{_bindir}/scmp_sys_resolver
%{_includedir}/seccomp.h
%{_includedir}/seccomp-syscalls.h
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc

%files help
%doc CHANGELOG CONTRIBUTING.md README.md
%{_mandir}/man*/*

%changelog
* Wed Jan 29 2025 Funda Wang <fundawang@yeah.net> - 2.6.0-1
- update to 2.6.0

* Fri May 10 2024 jchzhou <zhoujiacheng@iscas.ac.cn> - 2.5.4-3
- fix undefined behavior in scmp_bpf_sim.c causing issues when building with clang

* Mon Jul 31 2023 Jingyun Hua<huajingyun@loongson.cn> - 2.5.4-2
- add loongarch64 support for libseccomp

* Sat Jan 28 2023 shixuantong <shixuantong1@huawei.com> - 2.5.4-1
- upgrade version to 2.5.4

* Mon Nov 14 2022 shixuantong <shixuantong1@huawei.com> - 2.5.3-3
- arch: disambiguate in arch-syscall-validate

* Sat Aug 27 2022 zoulin <zoulin13@h-partners.com> - 2.5.3-2
- backport patches from upstream

* Tue Dec 28 2021 fuanan <fuanan3@huawei.com> - 2.5.3-1
- update version to 2.5.3

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.5.1-3
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Tue Jul 20 2021 fuanan <fuanan3@huawei.com> - 2.5.1-2
- Remove redundant gdb from BuildRequires

* Mon Jan 25 2021 wangchen <wangchen137@huawei.com> - 2.5.1-1
- update to 2.5.1

* Mon Jul 27 2020 Hugel <gengqihu1@huawei.com> - 2.5.0-1
- update to 2.5.0

* Tue Jun 30 2020 Liquor <lirui130@huawei.com> - 2.4.3-2
- add the patch of support for RISC-V

* Fri Apr 24 2020 BruceGW <gyl93216@163.com> - 2.4.3-1
- update upstream to 2.4.3

* Fri Oct 11 2019 jiangchuangang <jiangchuangang@huawei.com> - 2.4.1-3
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: remove so.* from devel

* Tue Sep 24 2019 luhuaxin <luhuaxin@huawei.com> - 2.4.1-2
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: add help package and merge static package

* Fri Aug 16 2019 luhuaxin <luhuaxin@huawei.com> - 2.4.1-1
- Package init
